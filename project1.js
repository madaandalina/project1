function Imobil(tip,proprietar,agent,locatie,pretVanzare,dimensiune,clientPotential,valoareOferta){
    this.tip=tip; 
    this.proprietar = proprietar,
     this.agent = agent,
     this.locatie = locatie,
     this.pretVanzare = pretVanzare,
     this.dimensiune = dimensiune,
     this.clientPotential = clientPotential,
     this.valoareOferta = valoareOferta
 
     this.show= function (){
         console.log(`locuinta de tip ${this.tip} este detinuta de ${this.proprietar}`);
     }


 }


function Casa(tip,proprietar,agent,locatie,pretVanzare,dimensiune,clientPotential,valoareOferta,nrCamere){
    Imobil.call(this,tip,proprietar,agent,locatie,pretVanzare,dimensiune,clientPotential,valoareOferta);
    this.nrCamere=nrCamere;  
}

function Apartament(tip,proprietar,agent,locatie,pretVanzare,dimensiune,clientPotential,valoareOferta,nrCamere,etaj){
    Imobil.call(this,tip,proprietar,agent,locatie,pretVanzare,dimensiune,clientPotential,valoareOferta);
    this.nrCamere=nrCamere;  
    this.etaj=etaj;
}

let tipuriImobile =[
 casa1=new Casa('casa','henry','maria','oradea',120_000,70,true,115_500,3),
 ap1= new Apartament('apartament','ana','Mihai','Bucuresti',80_000,60,false,70_000,3,1),
 ap2 =new Apartament('apartament','Marian',"Calin","Cluj-Napoca",65_000,56,true,60_000,2,9),
 casa2 = new Casa("casa","ana", "ionela", "oradea", 200_000, 120, true, 205_000),
 teren1 = new Imobil("teren","calin", "adela", "Cluj", 65_000, 50, false, 63_500)
];
     
 
 casa1.show();
 ap1.show();
 ap2.show();
 
 
 function Person( tip,adresa,nume,varsta,adresaEmail,telefon) {
     this.tip=tip,
     this.adresa = adresa,
     this.nume = nume,
     this.varsta = varsta,
     this.adresaEmail = adresaEmail,
     this.telefon = telefon
 
     this.show=function(){
         if(this.tip == "client"){
         console.log(`persoana de tip ${this.tip} are un buget de:${this.buget} `);
         }else{
         console.log(`persoana de tip ${this.tip} are adresa de e-mail ${this.adresaEmail}`);
         }
         
     }
   };
 
function Client ( tip,adresa,nume,varsta,adresaEmail,telefon,buget){
    Person.call(this,tip,adresa,nume,varsta,adresaEmail,telefon);
    this.buget=buget;
}

function Proprietar ( tip,adresa,nume,varsta,adresaEmail,telefon){
    Person.call(this,tip,adresa,nume,varsta,adresaEmail,telefon);
    
}

function Agent ( tip,adresa,nume,varsta,adresaEmail,telefon){
    Person.call(this,tip,adresa,nume,varsta,adresaEmail,telefon);
    
}

 let tipuriPersoane =[
     client1= new Client("client","Calarasi, nr 1","ana",35,"andreea@gmail.com",173748489,205_000),
     client2 = new Client("client","Bucuresti, nr 2", "henry",56,"cata@yahoo.com",487599603,70_000),
     proprietar1 = new Proprietar("proprietar","Iasi, nr 3", "Marin",36,"mariana@yahoo.com",484569603),
     agent1 = new Agent("agent","Ploiesti, nr 4", "Grigore",45,"grigore@yahoo.com",0740235452),
     agent2= new Agent("agent","Pitesti, nr 1", "Anca",25,"anca@yahoo.com",0740256462)
 ];
 
 
 
 client1.show();
 client2.show();
 agent1.show();
 proprietar1.show();
 
 //asignare
 let returnedTarget =Object.assign(tipuriImobile,tipuriPersoane.nume);
 console.log(returnedTarget);

 var iterator = tipuriImobile.entries();
 for(let e of iterator){
     console.log(e);
 }

 //tipuriImobile.forEach(element => console.log(tipuriImobile[element].clientPotential));

 for(let i=0; i<tipuriImobile.length;i++){
     for(let j=0;j<tipuriPersoane.length;j++){
         if(tipuriPersoane[j].tip == 'agent'){
            console.log(`agentului ${tipuriPersoane[j].nume} ii revine ${tipuriImobile[i].tip}`);
         }
         
     }
 }


 

 
 